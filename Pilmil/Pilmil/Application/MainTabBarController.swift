
import UIKit
import Photos

class MainTabBarController: UITabBarController {
    
    // User from LoginViewController
    var user: User!
    
    var imageArray: [Photo] = Array()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let browseVC = UIStoryboard.init(name: "Browse", bundle: nil).instantiateViewController(withIdentifier: "BrowseViewController") as! BrowseViewController
        let chatVC = UIStoryboard.init(name: "Chat", bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        let meVC = UIStoryboard.init(name: "Me", bundle: nil).instantiateViewController(withIdentifier: "MeViewController") as! MeViewController
        let sellVC = UIStoryboard.init(name: "Sell", bundle: nil).instantiateViewController(withIdentifier: "SellViewController") as! SellViewController
        
        // Add viewControllers to Tabbar
        viewControllers = [
            creatNavigationController(title: "Xem", image: "browse", selectedImage: "browse_selected", viewController: browseVC),
            creatNavigationController(title: "Tin nhắn", image: "chat", selectedImage: "chat_selected", viewController: chatVC),
            creatNavigationController(title: "Tôi", image: "me", selectedImage: "me_selected", viewController: meVC),
            creatNavigationController(title: "Bán", image: "sell", selectedImage: "sell_selected", viewController: sellVC)
        ]
        
        
        // Fetch photos from photo library
        let photos = PHPhotoLibrary.authorizationStatus()
        if photos == .notDetermined {
            PHPhotoLibrary.requestAuthorization({status in
                if status == .authorized{
                    self.grabPhotos()
                } else {
                    let alert = UIAlertController(title: "Photos Access Denied", message: "App needs access to photos library.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//                    self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                    self.present(alert, animated: true, completion: nil)
                }
            })
        } else if photos == .authorized {
            self.grabPhotos()
        }
        
//        let navController = viewControllers![2] as! UINavigationController
//        let vc = navController.topViewController as! MeViewController
//        vc.user = user

        guard let viewControllers = viewControllers else {
            return
        }

        for viewController in viewControllers {
            
            if let navigationController = viewController as? UINavigationController {
                if let viewController = navigationController.viewControllers.first as? BrowseViewController {
                    viewController.user  = user
                }
            }
            
            if let navigationController = viewController as? UINavigationController {
                if let viewController = navigationController.viewControllers.first as? ChatViewController {
                    viewController.user  = user
                }
            }
            
            if let navigationController = viewController as? UINavigationController {
                if let viewController = navigationController.viewControllers.first as? MeViewController {
                    viewController.user  = user
                }
            }

            if let sellNavigationController = viewController as? UINavigationController {
                if let viewController = sellNavigationController.viewControllers.first as? SellViewController {
                    viewController.imageArray  = imageArray
                    viewController.user  = user
                }
            }

        } // end class foreach loop
        
    } // end viewDidLoad
    
    private func creatNavigationController(title: String, image: String, selectedImage: String, viewController: UIViewController) -> UINavigationController {
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.tabBarItem.title   = title
        navigationController.tabBarItem.image   = UIImage(named: image)
        navigationController.tabBarItem.selectedImage = UIImage(named: selectedImage)
        return navigationController
    }
    
    func grabPhotos() {
        
        let imgManager = PHImageManager.default()
        
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = true
        requestOptions.deliveryMode = .highQualityFormat
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        
        let fetchResult: PHFetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
        if fetchResult.count > 0 {
            for i in 0..<fetchResult.count {
                imgManager.requestImage(for: fetchResult.object(at: i), targetSize: CGSize(width: 200, height: 200), contentMode: .aspectFill, options: requestOptions, resultHandler: { image, error in
                    let photo = Photo(id: (i + 1), image: image!)
                    self.imageArray.append(photo)
                })
            }
        } else {
            print("You got no photos")
        }
        
    } // end grabPhotos()

} // end class
