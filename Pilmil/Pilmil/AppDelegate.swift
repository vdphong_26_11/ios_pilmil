
import UIKit
import FacebookCore
import FacebookLogin
import Sentry
import SVProgressHUD
import SwiftyJSON

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        // Create a Sentry client and start crash handler
        do {
            Client.shared = try Client(dsn: "https://687c5542637844ec982520f4fd565069@sentry.io/1437348")
            try Client.shared?.startCrashHandler()
        } catch let error {
            print("SENTRY AppDelegate - \(error)")
            SVProgressHUD.showDismissableError(with: "\(error)")
        }
        
        // *Note: to user sentry again
        // 1. Uncheck 'Run script only when installing' in Run Script file at project targets Build Phases
        // 2. Using terminal, connect Sentry Server url -> https://sentry.io
        // by type in -> sentry-cli --url https://sentry.io/ login
        
        checkIfUserIsLoggedInWithFacebook()
        checkIfAppNeedToUpdate()
        setSVProgressHUD()
        setCustomNavigation()
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return SDKApplicationDelegate.shared.application(app, open: url, options: options)
    }
    
    // This is default function that replaced with above facebook guild for facebook sdk
//    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
//        // Override point for customization after application launch.
//
//        return true
//    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    
    /********** CUSTOM FUNCTIONS **********/
    
    func checkIfUserIsLoggedInWithFacebook() {
        
        // Get User by userId & Test login
        Request.getUser(userId: "999999") { completion in
            if !completion["user"].array!.isEmpty {
                let data: JSON   = completion["user"].array![0]
                let user = User(item: data)
                
                let rootViewController = UIStoryboard.init(name: "MainTabBar", bundle: nil).instantiateViewController(withIdentifier: "MainTabBarController") as! MainTabBarController
                rootViewController.user = user
                let share = UIApplication.shared.delegate as! AppDelegate
                share.window?.rootViewController = rootViewController
                share.window?.makeKeyAndVisible()
                
            } else {
                SVProgressHUD.showDismissableError(with: "User not exist")
            }
            
        }
        
        // Login with facebook
//        if let _ = AccessToken.current {
////            print("Start")
////            print(accessToken)
////            print("End")
//
//            if let savedLastUser = UserDefaults.standard.data(forKey: "lastUser") {
//                let decodedLastUser: User = try! JSONDecoder().decode(User.self, from: savedLastUser)
//
//                // Set MainTabBarController as RootViewController if user is already loggin with facebook
//                let rootViewController = UIStoryboard.init(name: "MainTabBar", bundle: nil).instantiateViewController(withIdentifier: "MainTabBarController") as! MainTabBarController
//                rootViewController.user = decodedLastUser
//                let share = UIApplication.shared.delegate as! AppDelegate
//                share.window?.rootViewController = rootViewController
//                share.window?.makeKeyAndVisible()
//            }
//        }
    } // end checkIfUserIsLoggedInWithFacebook()
    
    func checkIfAppNeedToUpdate() {
        let versionLocal = "\(Bundle.main.infoDictionary!["CFBundleShortVersionString"]!)"
        let appIdUrl     = ""
        
        VersionCheck.shared.checkAppStore() { isNew, versionStore, notes in
            
            if isNew != nil {
                if isNew! == true {
                    print("IS NEW VERSION AVAILABLE: \(isNew!), APP STORE VERSION: \(versionStore!)")
                    let dialogMessage = UIAlertController(title: "Phiên bản mới \(versionStore!)!", message: notes, preferredStyle: .alert)
                    
                    let update = UIAlertAction(title: "Cập nhật", style: .default, handler: { (action) -> Void in
                        if let url = URL(string: appIdUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!),
                            UIApplication.shared.canOpenURL(url){
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                            } else {
                                UIApplication.shared.openURL(url)
                            }
                        } else {
                            print("Can't Open URL")
                        }
                    })
                    
                    let cancel = UIAlertAction(title: "Không", style: .destructive, handler: nil)
                    
                    dialogMessage.addAction(update)
                    dialogMessage.addAction(cancel)
                    
                    // Ensure to show update notification only if local version is lower than store version
                    if versionLocal.compare(versionStore!, options: .numeric) == .orderedAscending {
                        self.window?.rootViewController!.present(dialogMessage, animated: true, completion: nil)
                    }
                    
                }
            }
            
        }
        
    } // end checkIfAppNeedToUpdate()
    
    func setSVProgressHUD() {
        SVProgressHUD.setDefaultStyle(.custom)
        SVProgressHUD.setDefaultMaskType(.black)
    }
    
    func setCustomNavigation() {
        let navigationBarAppearace = UINavigationBar.appearance()
        
        navigationBarAppearace.tintColor = UIColor.white
        navigationBarAppearace.barTintColor = Functions.returnUIColorFromHexString(hex: Colors.colorPrimary)
        navigationBarAppearace.backItem?.title = "               "
        
        // change navigation item title color
        navigationBarAppearace.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
    }

} // end AppDelegate

