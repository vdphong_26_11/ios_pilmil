
import Foundation
import UIKit

class SettingsItem {
    
    var title: String!
    var name: String!
    var groupId: Int!
    var groupName: String!
    var rightArrow: UIImage!
    
    init(name: String, title: String, groupId: Int, groupName: String) {
        self.title      = title
        self.name       = name
        self.groupId    = groupId
        self.groupName  = groupName
        self.rightArrow = UIImage(named: "right_chevron")
    }
    
    init() {
        //
    }

    func setTitle(title: String) { self.title = title }
    func getTitle() -> String { return self.title }

}
