
import Foundation
import UIKit
import SwiftyJSON

class SellItem {
    
    var userId: String
    var image: String
    var title: String
    var price: String
    var timeAgo: String
    var address: String
    var description: String
    var city: String
    var likes: String
    var isSold: String
    
    var imageLocal: UIImage
    
    init(item: JSON) {
        self.userId         = item["userId"].rawString()!
        self.image          = item["image"].rawString()!
        self.title          = item["title"].rawString()!
        self.price          = item["price"].rawString()!
        self.timeAgo        = item["timeAgo"].rawString()!
        self.address        = item["address"].rawString()!
        self.description    = item["description"].rawString()!
        self.city           = item["city"].rawString()!
        self.likes          = item["likes"].rawString()!
        self.isSold         = item["isSold"].rawString()!
        
        self.imageLocal     = UIImage(named: "11")!
    }
    
    
    init(userId: String, image: String, title: String, price: String, timeAgo: String, address: String, description: String, city: String, likes: String, isSold: String, imageLocal: UIImage) {
        self.userId         = userId
        self.image          = image
        self.title          = title
        self.price          = price
        self.timeAgo        = timeAgo
        self.address        = address
        self.description    = description
        self.city           = city
        self.likes          = likes
        self.isSold         = isSold
        
        self.imageLocal     = imageLocal
    }

} // end class
