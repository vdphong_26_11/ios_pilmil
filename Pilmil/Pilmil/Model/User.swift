
import Foundation
import SwiftyJSON

class User: NSObject, Codable {
    
    var userId: String      = ""
    var username: String    = ""
    var userName: String    = ""
    var email: String       = ""
    var createDate: String  = ""
    
    override init() {
        //
    }
    
    init(userId: String, username: String, email: String, userName: String, createDate: String) {
        self.username   = username
        self.userId     = userId
        self.email      = email
        self.userName   = userName
        self.createDate = createDate
    }
    
    init(item: JSON) {
        self.userId     = item["userId"].rawString()!
        self.username   = item["username"].rawString()!
        self.userName   = item["user_name"].rawString()!
        self.email      = item["email"].rawString()!
        self.createDate = item["createDate"].rawString()!
    }
    
    func getUsername() -> String {
        return self.username
    }
    
    func getUserId() -> String {
        return self.userId
    }
    
}
