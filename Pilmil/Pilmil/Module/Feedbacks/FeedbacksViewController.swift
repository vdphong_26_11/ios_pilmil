
import UIKit

class FeedbacksViewController: UIViewController {
    
    @IBOutlet var btnBack: UIButton!
    
    @IBOutlet var containerViewAll: UIView!
    @IBOutlet var containerViewLikes: UIView!
    @IBOutlet var containerViewDislikes: UIView!
    @IBOutlet var segmentedControl: UISegmentedControl!
    
    var feedbackAllList: [Feedback] = Array()
    var feedbackLikesList: [Feedback] = Array()
    var feedbackDislikesList: [Feedback] = Array()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Functions.customBackButton(backButton: btnBack)
        
        segmentedControl.setTitle("Tất cả (\(feedbackAllList.count))", forSegmentAt: 0)
        segmentedControl.setTitle("Thích (\(feedbackLikesList.count))", forSegmentAt: 1)
        segmentedControl.setTitle("Không thích (\(feedbackDislikesList.count))", forSegmentAt: 2)

    } // end viewDidLoad()
    
    // Send user object data to DevicelistViewController
    var boolOnce = true
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // feedbackList execute here because prepareForSegue() is called before viewDidLoad()
        if boolOnce { // make sure prepareForSegue()
            feedbackAllList = returnFeedbackList()
            boolOnce = false
        }
        
        if segue.identifier == "segueFeedbacksToAll" {
            let childViewController = segue.destination as! AllViewController
            childViewController.feedbackList = feedbackAllList
        } else if segue.identifier == "segueFeedbacksToLikes" {
            let childViewController = segue.destination as! LikesViewController
            childViewController.feedbackList = feedbackLikesList
        } else if segue.identifier == "segueFeedbacksToDislikes" {
            let childViewController = segue.destination as! DislikesViewController
            childViewController.feedbackList = feedbackDislikesList
        }
    
    } // end prepare()
    
    @IBAction func segmentedChange(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            containerViewAll.alpha      = 1
            containerViewLikes.alpha    = 0
            containerViewDislikes.alpha = 0
            break
            
        case 1:
            containerViewAll.alpha      = 0
            containerViewLikes.alpha    = 1
            containerViewDislikes.alpha = 0
            break
            
        case 2:
            containerViewAll.alpha      = 0
            containerViewLikes.alpha    = 0
            containerViewDislikes.alpha = 1
            break
            
        default:
            break
        }
        
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    private func returnFeedbackList() -> [Feedback] {
        var list: [Feedback] = Array()
        for i in 0...2 {
            
            switch i {
                
            case 0:
                let feedback = Feedback(userPhoto: UIImage(named: "myPhoto")!, username: "Ngọc", feedback: "Bán hàng tốt, thân thiệt!", date: "11/12/2018", type: "like")
                list.append(feedback)
                feedbackLikesList.append(feedback)
                break
                
            case 1:
                let feedback = Feedback(userPhoto: UIImage(named: "myPhoto")!, username: "Nam", feedback: "Bán hàng tốt, thân thiệt 1!", date: "07/12/2018", type: "like")
                list.append(feedback)
                feedbackLikesList.append(feedback)
                break
                
            case 2:
                let feedback = Feedback(userPhoto: UIImage(named: "myPhoto")!, username: "Thảo", feedback: "Bán hàng tốt, thân thiệt 2!", date: "05/12/2018", type: "dislike")
                list.append(feedback)
                feedbackDislikesList.append(feedback)
                break
                
            default:
                break
                
            }
            
        }
        return list
    }
    
} // end class

class FeedbackTableViewCell: UITableViewCell {
    
    var feedback: Feedback!
    
    @IBOutlet var imageViewUser: UIImageView!
    @IBOutlet var labelUsername: UILabel!
    @IBOutlet var labelFeedback: UILabel!
    @IBOutlet var labelFeedbackTime: UILabel!
    @IBOutlet var imageViewFace: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        Functions.customImageViewUser(imageView: imageViewUser)
        
    }
    
    func setData(feedback: Feedback) {
        
        self.feedback = feedback
        
        imageViewUser.image     = feedback.userPhoto
        labelUsername.text      = feedback.username
        labelFeedback.text      = feedback.feedback
        labelFeedbackTime.text  = feedback.date
        
        switch feedback.type {
        case "like":
            let image = UIImage(named: "happy")?.withRenderingMode(.alwaysTemplate)
            imageViewFace.image = image
            imageViewFace.tintColor = Functions.returnUIColorFromHexString(hex: Colors.colorPrimary)
            break
            
        case "dislike":
            let image = UIImage(named: "neutral")?.withRenderingMode(.alwaysTemplate)
            imageViewFace.image = image
            imageViewFace.tintColor = UIColor.red
            break
            
        default:
            break
        }
        
    }
    
} // end class FeedbackTableViewCell

class Feedback {
    
    var userPhoto: UIImage
    var username: String
    var feedback: String
    var date: String
    var type: String
    
    init(userPhoto: UIImage, username: String, feedback: String, date: String, type: String) {
        self.userPhoto  = userPhoto
        self.username   = username
        self.feedback   = feedback
        self.date       = date
        self.type       = type
    }
    
} // end class
