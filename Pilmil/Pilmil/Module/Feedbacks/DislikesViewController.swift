
import UIKit

class DislikesViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    var feedbackList: [Feedback] = Array()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        
    }
    
}

extension DislikesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedbackList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let feedback = self.feedbackList[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedbackTableViewCell", for: indexPath) as! FeedbackTableViewCell
        cell.setData(feedback: feedback)
        return cell
    }
    
} // end extension
