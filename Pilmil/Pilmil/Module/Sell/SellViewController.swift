

import UIKit
import Photos

class SellViewController: UIViewController {
    
    var user: User!
    
    // Data get from MainTabbarController
    var imageArray: [Photo] = Array()
    
    // Data get from PostViewController
    var boolFromPostViewController: Bool = false
    
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var collectionView: UICollectionView!
    
    var myCollectionView: UICollectionView!
    
    // Variables
    var imageArrayPicked: [Photo] = Array()
    var cellArray: [PhotoCell] = Array()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Functions.setViewBackgroundColor(view: self.view)
        Functions.setNavigationPrimaryBarTint(navigationController: navigationController!)
        
        // Set default next button to be unable to continue
        Functions.customDefaultSellButton(button: btnNext)
        
        // SET UP PHOTOS FROM GALLERY
        collectionView.allowsMultipleSelection = true
        collectionView.dataSource   = self
        collectionView.delegate     = self
        
        // Check if view is visible
//        if self.isViewLoaded && (self.view.window != nil) {
//            print("VIEW VISIBLE")
//        } else {
//            print("VIEW INVISIBLE")
//        }
        
    } // end viewDidLoad
    
    
    // Custom Funcitons
    private func enableButtonNext() {
        btnNext.isUserInteractionEnabled = true
        btnNext.setTitleColor(UIColor.white, for: .normal)
    }
    
    private func disableButtonNext() {
        btnNext.isUserInteractionEnabled = false
        if #available(iOS 11.0, *) {
            btnNext.setTitleColor(UIColor(named: "colorLightGrey"), for: .normal)
        } else {
            btnNext.setTitleColor(Functions.returnUIColorFromHexString(hex: "#BABABA"), for: .normal)
        }
    }
    
    @IBAction func btnNext(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "Post", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "PostViewController") as! PostViewController
        viewController.imageArrayPicked = self.imageArrayPicked
        navigationController?.pushViewController(viewController, animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        if boolFromPostViewController == true {
            collectionView.reloadData()
            imageArrayPicked.removeAll()
            disableButtonNext()
            boolFromPostViewController = false
        }
    }
    
} // end class

extension SellViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let photo = imageArray[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
        cell.setData(photo: photo)
        return cell
    
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let photo = imageArray[indexPath.row]
        let cell = collectionView.cellForItem(at: indexPath) as! PhotoCell
        if imageArrayPicked.count < 3 {
            cellArray.append(cell)
            imageArrayPicked.append(photo)
            print("selected \(imageArrayPicked.count)")
            loadIndex(cellArray: cellArray)
            cell.isSelected = true
        }
        enableButtonNext()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let photo = imageArray[indexPath.row]
        let cell = collectionView.cellForItem(at: indexPath) as! PhotoCell
        cell.isSelected = false
        if let index = imageArrayPicked.firstIndex(where: { $0 === photo}) {
            cellArray.remove(at: index)
            imageArrayPicked.remove(at: index)
            print("deselected \(imageArrayPicked.count)")
            loadIndex(cellArray: cellArray)
            if imageArrayPicked.count == 0 {
                disableButtonNext()
            }
        }

    }

    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        if imageArrayPicked.count == 3 {
            return false
        } else {
            return true
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 3 - 1
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    private func loadIndex(cellArray: [PhotoCell]) {
        var i = 0
        for cell in cellArray {
            i += 1
            cell.labelCount.text = String(i)
        }
    }
    
} // end extension

class PhotoCell: UICollectionViewCell {
    
    var photo: Photo!
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var labelCount: UILabel!
    
    func setData(photo: Photo) {
        self.photo = photo
        
        imageView.image = photo.image
        labelCount.isHidden = true
        labelCount.layer.cornerRadius = labelCount.frame.height / 2
        
    } // end setData()
    
    override var isSelected: Bool {
        get {
            return super .isSelected
        } set {
            if (super.isSelected != newValue) {
                super.isSelected = newValue
                
                if (newValue == true) {
//                    labelCount.isHidden = false
                    imageView.layer.opacity = 0.7
                    customCellSelected(cell: self)
                } else {
//                    labelCount.isHidden = true
                    imageView.layer.opacity = 1
                    customCellDeselected(cell: self)
                }
            }
        }
    }
    
    private func customCellSelected(cell: UICollectionViewCell) {
        cell.layer.cornerRadius = 5.0
        cell.layer.borderWidth = 2.0
        if #available(iOS 11.0, *) {
            cell.layer.borderColor = UIColor(named: "colorPrimary")?.cgColor
        } else {
            cell.layer.borderColor = Functions.returnUIColorFromHexString(hex: "#2FA705").cgColor
        }
    }
    
    private func customCellDeselected(cell: UICollectionViewCell) {
        cell.layer.cornerRadius = 0
        cell.layer.borderWidth = 0
        cell.layer.borderColor = nil
    }
    
} // end class PhotoCell


//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let width = collectionView.frame.width
//        //        if UIDevice.current.orientation.isPortrait {
//        //            return CGSize(width: width/4 - 1, height: width/4 - 1)
//        //        } else {
//        //            return CGSize(width: width/6 - 1, height: width/6 - 1)
//        //        }
//        if DeviceInfo.Orientation.isPortrait {
//            return CGSize(width: width/4 - 1, height: width/4 - 1)
//        } else {
//            return CGSize(width: width/6 - 1, height: width/6 - 1)
//        }
//    }
//
//    override func viewWillLayoutSubviews() {
//        super.viewWillLayoutSubviews()
//        myCollectionView.collectionViewLayout.invalidateLayout()
//    }


//struct DeviceInfo {
//    struct Orientation {
//        // indicate current device is in the LandScape orientation
//        static var isLandscape: Bool {
//            get {
//                return UIDevice.current.orientation.isValidInterfaceOrientation
//                    ? UIDevice.current.orientation.isLandscape
//                    : UIApplication.shared.statusBarOrientation.isLandscape
//            }
//        }
//        // indicate current device is in the Portrait orientation
//        static var isPortrait: Bool {
//            get {
//                return UIDevice.current.orientation.isValidInterfaceOrientation
//                    ? UIDevice.current.orientation.isPortrait
//                    : UIApplication.shared.statusBarOrientation.isPortrait
//            }
//        }
//    }
//}

class Photo {
    var id: Int
    var image: UIImage
    
    init(id: Int, image: UIImage) {
        self.id = id
        self.image = image
    }
    
}
