

import UIKit

class PostViewController: UIViewController {
    
    // Variables get from SellViewController
    var imageArrayPicked: [Photo] = Array()
    var boolFromSellViewController: Bool = true
    
    @IBOutlet var viewContainer: UIView!
    @IBOutlet var btnSell: UIButton!
    
    @IBOutlet var viewTitle: UIView!
    @IBOutlet var textFieldTitle: UITextField!
    @IBOutlet var ivTickTitle: UIImageView!
    
    @IBOutlet var viewPrice: UIView!
    @IBOutlet var labelPriceCurrency: UILabel!
    @IBOutlet var textFieldPrice: UITextField!
    @IBOutlet var ivTickPrice: UIImageView!
    
    @IBOutlet var viewCity: UIView!
    @IBOutlet var textFieldCity: UITextField!
    @IBOutlet var ivTickCity: UIImageView!
    
    @IBOutlet var viewAddress: UIView!
    @IBOutlet var textFieldAddress: UITextField!
    @IBOutlet var ivTickAddress: UIImageView!
    
    @IBOutlet var viewDescription: UIView!
    @IBOutlet var textViewDescription: UITextView!
    @IBOutlet var ivTickDescription: UIImageView!
    
    @IBOutlet var collectionView: UICollectionView!
    
    // Bool textfields filled
    var boolTitle: Bool   = false
    var boolPrice: Bool   = false
    var boolCity: Bool    = false
    var boolAddress: Bool = false
    var boolDescription   = false
    var boolArray = [String: Bool]()
    
    // Variables
    let placeholderText = "Miêu tả đồ muốn bạn đang bán là gì, như thế nào, etc..."
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Functions.setViewBackgroundColor(view: self.view)
        Functions.setViewBackgroundColor(view: self.viewContainer)
        
        // Set custom view input
        cucstomViewInput(view: viewTitle)
        cucstomViewInput(view: viewPrice)
        labelPriceCurrency.attributedText = NSAttributedString(string: "đ", attributes:
            [.underlineStyle: NSUnderlineStyle.single.rawValue])
        cucstomViewInput(view: viewCity)
        cucstomViewInput(view: viewAddress)
        cucstomViewInput(view: viewDescription)
//        viewDescription.layer.borderWidth = 0.5
        
        // Set custom TextField / TextView input
        textFieldTitle.addTarget(self, action: #selector(editingTextFieldTitle(sender:) ), for: .allEditingEvents)
        textFieldPrice.addTarget(self, action: #selector(editingTextFieldPrice(sender:) ), for: .allEditingEvents)
        textFieldCity.addTarget(self, action: #selector(editingTextFieldCity(sender:) ), for: .allEditingEvents)
        textFieldAddress.addTarget(self, action: #selector(editingTextFieldAddress(sender:) ), for: .allEditingEvents)
        textFieldPrice.keyboardType = .numberPad
        textViewDescription.delegate = self
        textViewDescription.text = placeholderText
        textViewDescription.textColor = Functions.returnUIColorFromHexString(hex: "#BABABA")
        
        // View tap gesture
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
//        self.view.addGestureRecognizer(tapGesture)

        
        // Set default next button to be unable to continue
        Functions.customDefaultSellButton(button: btnSell)
        if #available(iOS 11.0, *) {
            btnSell.layer.backgroundColor = UIColor(named: "colorPrimaryDark")?.cgColor
        } else {
            btnSell.layer.backgroundColor = Functions.returnUIColorFromHexString(hex: "#247F05").cgColor
        }
        
        // Set collection view
        collectionView.dataSource = self
        collectionView.delegate   = self
        
        // Initiate Bool All Filled Array
        boolArray = [
            "boolTitle" : false,
            "boolPrice" : false,
            "boolCity"  : false,
            "boolAddress" : false,
            "boolDescription" : false
        ]
        
    } // end viewDidLoad
    
    @IBAction func btnSell(_ sender: UIButton) {
        let viewController = self.navigationController!.viewControllers[0] as! SellViewController
        viewController.boolFromPostViewController = boolFromSellViewController
        self.navigationController?.popToRootViewController(animated: true)
        print("Sell success")
    }
    
    // Custom functions
    @objc func editingTextFieldTitle(sender: AnyObject) {
        let count = textFieldTitle.text?.count
        if count! >= 1 {
            ivTickTitle.image = UIImage(named: "ticked")
            boolTitle = true
        } else {
            ivTickTitle.image = UIImage(named: "tick")
            boolTitle = false
        }
        boolArray.updateValue(boolTitle, forKey: "boolTitle")
        validateTextFields(boolArray: boolArray)
    }
    
    @objc func editingTextFieldPrice(sender: AnyObject) {
        let count = textFieldPrice.text?.count
        if count! >= 1 {
            ivTickPrice.image = UIImage(named: "ticked")
            boolPrice = true
        } else {
            ivTickPrice.image = UIImage(named: "tick")
            boolPrice = false
        }
        boolArray.updateValue(boolPrice, forKey: "boolPrice")
        validateTextFields(boolArray: boolArray)
    }
    
    @objc func editingTextFieldCity(sender: AnyObject) {
        let count = textFieldCity.text?.count
        if count! >= 1 {
            ivTickCity.image = UIImage(named: "ticked")
            boolCity = true
        } else {
            ivTickCity.image = UIImage(named: "tick")
            boolCity = false
        }
        boolArray.updateValue(boolCity, forKey: "boolCity")
        validateTextFields(boolArray: boolArray)
    }
    
    @objc func editingTextFieldAddress(sender: AnyObject) {
        let count = textFieldAddress.text?.count
        if count! >= 1 {
            ivTickAddress.image = UIImage(named: "ticked")
            boolAddress = true
        } else {
            ivTickAddress.image = UIImage(named: "tick")
            boolAddress = false
        }
        boolArray.updateValue(boolAddress, forKey: "boolAddress")
        validateTextFields(boolArray: boolArray)
    }

    private func validateTextFields(boolArray: [String : Bool]) {
        if boolArray.count == 5 {
            let check1 = boolArray["boolTitle"] == true
            let check2 = boolArray["boolPrice"] == true
            let check3 = boolArray["boolCity"] == true
            let check4 = boolArray["boolAddress"] == true
            let check5 = boolArray["boolDescription"] == true
            
            if check1 && check2 && check3 && check4 && check5 {
                enableButtonSell(button: btnSell)
            } else {
                disableButtonSell(button: btnSell)
            }
        }
    }
    
    @objc func viewTapped() {
        self.view.endEditing(true)
    }
    
    private func cucstomViewInput(view: UIView) {
        view.layer.addBorder(edge: .top, color: UIColor.lightGray, thickness: 0.5)
        view.layer.addBorder(edge: .bottom, color: UIColor.lightGray, thickness: 0.5)
    }
    
    private func enableButtonSell(button: UIButton) {
        button.isUserInteractionEnabled = true
        button.setTitleColor(UIColor.white, for: .normal)
    }
    
    private func disableButtonSell(button: UIButton) {
        button.isUserInteractionEnabled = false
        if #available(iOS 11.0, *) {
            button.setTitleColor(UIColor(named: "colorLightGrey"), for: .normal)
        } else {
            button.setTitleColor(Functions.returnUIColorFromHexString(hex: "#BABABA"), for: .normal)
        }
    }
    
} // end class

extension PostViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArrayPicked.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let photo = imageArrayPicked[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PickedPhotoCell", for: indexPath) as! PickedPhotoCell
        cell.setData(photo: photo)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let viewController = storyboard?.instantiateViewController(withIdentifier: "PhotosPreviewViewController") as! PhotosPreviewViewController
//        viewController.imageArrayPicked = imageArrayPicked
//        self.present(viewController, animated: true, completion: nil
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = collectionView.frame.height / 1
        return CGSize(width: height, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
} // end extension


extension CALayer {
    
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer();
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect(x:0, y:self.frame.height - thickness, width:self.frame.width, height:thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect(x:0, y:0, width: thickness, height: self.frame.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect(x:self.frame.width - thickness, y: 0, width: thickness, height:self.frame.height)
            break
        default:
            break
        }
        
        border.backgroundColor = color.cgColor;
        
        self.addSublayer(border)
    }
    
} // end extension

extension PostViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView){
        
        if (textView.text == placeholderText)
        {
            textView.text = ""
            textView.textColor = Functions.returnUIColorFromHexString(hex: "#0078B9")
        }
        textView.becomeFirstResponder() //Optional
  
    }
    
    func textViewDidEndEditing(_ textView: UITextView){
        if (textView.text == "")
        {
            textView.text = placeholderText
            textView.textColor = Functions.returnUIColorFromHexString(hex: "#BABABA")
        }
        textView.resignFirstResponder()
    }
    
    // TextView Description Character Count
    func textViewDidChange(_ textView: UITextView) {
        let count = textView.text.count
        if count >= 1 {
            ivTickDescription.image = UIImage(named: "ticked")
            boolDescription = true
        } else {
            ivTickDescription.image = UIImage(named: "tick")
            boolDescription = false
        }
        boolArray.updateValue(boolDescription, forKey: "boolDescription")
        validateTextFields(boolArray: boolArray)
    }
    
} // end extension

class PickedPhotoCell: UICollectionViewCell {
    
    var photo: Photo!
    
    @IBOutlet var imageView: UIImageView!
    
    func setData(photo: Photo) {
        self.photo = photo
        
        imageView.image = photo.image
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 5.0
        
    } // end setData()
    
}
