

import UIKit

class SettingsItemCell: UITableViewCell {
    
    var item: SettingsItem!
    
    @IBOutlet var title: UILabel!
    @IBOutlet var imageViewRightChevron: UIImageView!
    
    func setData(item: SettingsItem) {
        
        self.item       = item
        self.title.text = item.title
        
        if item.name != "version" && item.name != "logout" {
            self.imageViewRightChevron.image = UIImage(named: "right_chevron")?.withRenderingMode(.alwaysTemplate)
            self.imageViewRightChevron.tintColor = UIColor.lightGray
        }
        
        
        if item.name == "logout" {
            self.title.textColor = UIColor.red
            self.title.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.medium)
            self.imageViewRightChevron.image = UIImage(named: "logout")?.withRenderingMode(.alwaysTemplate)
            self.imageViewRightChevron.tintColor = UIColor.lightGray
        }
        
    }
}
