
import UIKit
import FacebookLogin
import SVProgressHUD

class SettingsViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var btnBack: UIButton!
    
    var sortedGroupHasMap = [(key: Int, value: [SettingsItem])]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Functions.customBackButton(backButton: btnBack)
        
        tableView.dataSource = self
        tableView.delegate   = self
        
        // CREATE ITEMS TO LIST IN TABLE VIEW
        let settingsItemsList: [SettingsItem] = [
            SettingsItem(name: "profile", title: "Thông tin tài khoản", groupId: 1, groupName: "Cài đặt tài khoản"),
            SettingsItem(name: "changePassword", title: "Đổi mật khẩu", groupId: 1, groupName: "Cài đặt tài khoản"),
            SettingsItem(name: "version", title: "Phiên bản  (\(Bundle.main.infoDictionary!["CFBundleShortVersionString"]!))", groupId: 2, groupName: "Thông tin ứng dụng"),
            SettingsItem(name: "logout", title: "Đăng xuất", groupId: 3, groupName: ""),
        ]
        
        
        var hashedMapGroup: [Int: [SettingsItem]] = [:]
        
        for i in 0..<settingsItemsList.count {
            
            let item = settingsItemsList[i]
            if (hashedMapGroup.keys.contains(item.groupId)) {
                var existedItem = hashedMapGroup[item.groupId]
                existedItem?.append(item)
                hashedMapGroup.updateValue(existedItem!, forKey: item.groupId)
            } else {
                var newItemList: [SettingsItem] = Array()
                newItemList.append(item)
                hashedMapGroup.updateValue(newItemList, forKey: item.groupId)
            }
        }
        
        sortedGroupHasMap = hashedMapGroup.sorted(by: { $0.key < $1.key })
        
    } // end viewDidLoad()
    
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
} // end class

extension SettingsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sortedGroupHasMap[section].value[0].groupName
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sortedGroupHasMap.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortedGroupHasMap[section].value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsItemCell", for: indexPath) as! SettingsItemCell
        let item = sortedGroupHasMap[indexPath.section].value[indexPath.row]
        cell.setData(item: item)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = sortedGroupHasMap[indexPath.section].value[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        if item.name == "logout" {

            // Declare Alert
            let dialogMessage = UIAlertController(title: "", message: "Bạn có chắc chắn muốn đăng xuất?", preferredStyle: .alert)
            
            // Create OK button with action handler
            let confirm = UIAlertAction(title: "Có", style: .default, handler: { (action) -> Void in
                
                SVProgressHUD.show()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { // Change `2.0` to the desired number of seconds.
                    LoginManager().logOut()
                    let rootViewController = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    let share = UIApplication.shared.delegate as! AppDelegate
                    share.window?.rootViewController = rootViewController
                    share.window?.makeKeyAndVisible()
                    
                    SVProgressHUD.dismiss()
                }
                
            })
            
            // Create Cancel button with action handlder
            let cancel = UIAlertAction(title: "Không", style: UIAlertAction.Style.destructive, handler: nil)
            
            //Add OK and Cancel button to dialog message
            dialogMessage.addAction(confirm)
            dialogMessage.addAction(cancel)
            
            // Present dialog message to user
            self.present(dialogMessage, animated: true, completion: nil)
            
        }
        
    }
    
} // end extension
