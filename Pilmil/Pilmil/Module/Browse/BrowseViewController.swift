
import UIKit
import SVProgressHUD
import SwiftyJSON

class BrowseViewController: UIViewController {
    
    var user: User!
    
    // Variables connected from storyboard
    @IBOutlet var collectionViewBrowse: UICollectionView!
    @IBOutlet var viewSearchBar: UIView!
    @IBOutlet var imageViewSearch: UIImageView!
    @IBOutlet var viewSearch: UIView!
    @IBOutlet var textFieldSearch: UITextField!
    @IBOutlet var btnCancelSearch: UIButton!
    @IBOutlet var widthBtnCancelSearch: NSLayoutConstraint!
    
    // Variables
    var sellItemList: [SellItem] = Array()
    
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = Functions.returnUIColorFromHexString(hex: Colors.colorPrimary)
        refreshControl.backgroundColor = Functions.returnUIColorFromHexString(hex: Colors.colorBackground)
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add Refresh Control to UICollectionView & Configure Refresh Control
        collectionViewBrowse.refreshControl = refresher

        // Custom View Search
        viewSearch.backgroundColor = Functions.returnUIColorFromHexString(hex: Colors.colorPrimary)

        // Custom TextField Search
        customTextFieldSearch(textField: textFieldSearch)

        // Custom button Cancel Search
        widthBtnCancelSearch.constant = 0

        // Custom viewSearchBar
        let searchImage = UIImage(named: "search")?.withRenderingMode(.alwaysTemplate)
        imageViewSearch.image = searchImage
        imageViewSearch.contentMode = .scaleAspectFit
        imageViewSearch.tintColor = UIColor.lightGray
        viewSearchBar.layer.cornerRadius = viewSearchBar.frame.height / 2

        Functions.setViewBackgroundColor(view: collectionViewBrowse)
        collectionViewBrowse?.contentInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        collectionViewBrowse.dataSource = self
        collectionViewBrowse.delegate   = self
        
        refresher.beginRefreshing()
        DispatchQueue.main.asyncAfter(deadline: .now() + GlobalVariables.delayRequest) {
            self.getItemsRequest()
        }
        
    } // end viewDidLoad

    @IBAction func btnCancelSearch(_ sender: UIButton) {
        self.view.endEditing(true)
        hideBtnCancelSearch()
    }
    
    @IBAction func tfSearchTouchDown(_ sender: UITextField) {
        showBtnCancelSearch()
    }
    
    private func hideBtnCancelSearch() {
        imageViewSearch.tintColor = UIColor.lightGray
        UIView.animate(withDuration: 0.1, animations: {
            self.widthBtnCancelSearch.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
    private func showBtnCancelSearch() {
        imageViewSearch.tintColor = UIColor.white
        UIView.animate(withDuration: 0.1, animations: {
            self.widthBtnCancelSearch.constant = 62
            self.view.layoutIfNeeded()
        })
    }
    
    
    // Custom Functions
    private func customTextFieldSearch(textField: UITextField) {

        textField.attributedPlaceholder = NSAttributedString(string: "Bạn muốn tìm gì?",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        let clearButton = UIButton(type: .custom)
        clearButton.setImage(UIImage(named: "clear_button")!, for: .normal)
        clearButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        clearButton.contentMode = .scaleAspectFit
        clearButton.addTarget(self, action: #selector(clear(sender:) ), for: .touchUpInside)
        textField.addTarget(self, action: #selector(editing(sender:) ), for: .allEditingEvents)
        textField.rightView = clearButton
        textField.rightViewMode = .always
        textField.rightView?.isHidden = true
        
    } // end customTextFieldSearch()
    
    @objc func clear(sender : AnyObject) {
        textFieldSearch.text = ""
        textFieldSearch.rightView?.isHidden = true
    }

    @objc func editing(sender: AnyObject) {
        let count = textFieldSearch.text?.count
        if count! >= 1 {
            textFieldSearch.rightView?.isHidden = false
        } else {
            textFieldSearch.rightView?.isHidden = true
        }
    }
    
    @objc private func refreshItems() {
        self.perform(#selector(refreshItemsRequest(_:)), with: nil, afterDelay: GlobalVariables.delayRefresher)
    }
    
    @objc private func refreshItemsRequest(_ sender: Any) {
        getItemsRequest()
    }
    
    private func getItemsRequest() {
        
        Request.getSellItems(viewController: self) { completion in

            print("Browse requested")
            self.sellItemList.removeAll()

            let data: JSON      = completion["data"]
            let items: [JSON]   = data["items"].array!
            
            if (!items.isEmpty || !(items.count == 0)) {
                for i in 0..<items.count {
                    
                    let item  = SellItem(item: items[i])
                    self.sellItemList.append(item)
                    
                } // end for-loop
            } else {
                Functions.createLabel(text: "Không có gì được bán", view: self.collectionViewBrowse)
            }

            self.refresher.endRefreshing()
            self.collectionViewBrowse.reloadData()

        }

//        Request.trackingItemsRequest(viewController: self, username: username, token: token, userId: userId, TRACKING_ITEMS_URL: TRACKING_ITEMS_URL) { completion in
//
//            print("-----START \n \(completion) \n END-----")
//
//            SVProgressHUD.dismiss()
//
//        }

    } // end getItemsRequest()
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        super.viewWillDisappear(animated)
    }

} // end class

extension BrowseViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sellItemList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(UINib(nibName: "ItemCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ItemCollectionViewCell")
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemCollectionViewCell", for: indexPath) as! ItemCollectionViewCell
        cell.sellItem = sellItemList[indexPath.item]
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let sellItem = sellItemList[indexPath.row]
        let storyboard = UIStoryboard.init(name: "SingleItem", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SingleItemViewController") as! SingleItemViewController
        viewController.sellItem = sellItem
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let phoneSize   = (collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right + 8)) / 2
        
        return CGSize(width: phoneSize, height: phoneSize * CGFloat(GlobalVariables.heightFactor))
        
//        let sizePad     = (collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right + 8)) / 3
//
//        if (UIDevice.current.userInterfaceIdiom == .pad) {
//            return CGSize(width: sizePad, height: sizePad * CGFloat(GlobalVariables.heightFactor))
//        } else {
//            return CGSize(width: phoneSize, height: phoneSize * CGFloat(GlobalVariables.heightFactor))
//        }
     
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 7
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 7
    }
    
} // end extension

extension BrowseViewController: SellItemCellDelegate {
    func didTapButtonLike(item: SellItem) {
        //
    }
    
    func didTapButtonUser(item: SellItem) {
        //
    }
    
} // end extension

