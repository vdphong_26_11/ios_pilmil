
import UIKit

class ItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet fileprivate weak var viewContainer: UIView!
    @IBOutlet fileprivate weak var imageViewItem: UIImageView!
    @IBOutlet fileprivate weak var labelTitle: UILabel!
    @IBOutlet fileprivate weak var labelPrice: UILabel!
    @IBOutlet var labelTimeAgo: UILabel!
    @IBOutlet var labelCity: UILabel!
    @IBOutlet var btnLike: UIButton!
    @IBOutlet var imageViewByUserPhoto: UIImageView!
    @IBOutlet var labelIsSold: UILabel!
    
    var delegate: SellItemCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewContainer.layer.cornerRadius = 6
        viewContainer.layer.masksToBounds = true
        viewContainer.layer.borderWidth = 0.2
        if #available(iOS 11.0, *) {
            viewContainer.layer.borderColor = UIColor(named: "colorLighestGrey")?.cgColor
        } else {
            viewContainer.layer.borderColor = UIColor.lightGray.cgColor
        }
        
        let cornerRadius = imageViewByUserPhoto.frame.height
        imageViewByUserPhoto.layer.cornerRadius = cornerRadius / 2
        imageViewByUserPhoto.layer.borderWidth  = 0.2
        imageViewByUserPhoto.layer.borderColor  = UIColor.lightGray.cgColor
        
        // Set labelIsSold bottom Left corner radius only
        let bounds: CGRect = labelIsSold.bounds
        let maskPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: ([.bottomLeft]), cornerRadii: CGSize(width: 7.0, height: 7.0))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = maskPath.cgPath
        labelIsSold.layer.mask = maskLayer
        labelIsSold.isHidden = true
        
    }
    
    var sellItem: SellItem? {
        didSet {
            if let sellItem = sellItem {
                imageViewItem.image = sellItem.imageLocal
                labelTitle.text     = sellItem.title
                labelPrice.text     = sellItem.price
                labelTimeAgo.text   = sellItem.timeAgo
                labelCity.text      = sellItem.city
                btnLike.setTitle(sellItem.likes, for: .normal)
                
                if sellItem.isSold == "true" {
                    labelIsSold.isHidden = false
                } else {
                    labelIsSold.isHidden = true
                }
                
            }
        }
    }
    
    @IBAction func btnLike(_ sender: UIButton) {
        delegate?.didTapButtonLike(item: sellItem!)
        
        let addCount = Int(sellItem!.likes)! + 1
        btnLike.setTitle("\(addCount)", for: .normal)

        if GlobalVariables.boolDidLikeItem == false {
            btnLike.setImage(UIImage(named: "liked"), for: .normal)
        } else if GlobalVariables.boolDidLikeItem == true {
            btnLike.setImage(UIImage(named: "like"), for: .normal)
        }
        
        GlobalVariables.boolDidLikeItem = !GlobalVariables.boolDidLikeItem
        
    }
    
    @IBAction func btnUser(_ sender: Any) {
        delegate?.didTapButtonUser(item: sellItem!)
    }
    
} // end class

protocol SellItemCellDelegate {
    func didTapButtonLike(item: SellItem)
    func didTapButtonUser(item: SellItem)
}
