
import UIKit

class ChatViewController: UIViewController {
    
    var user: User!
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = Functions.returnUIColorFromHexString(hex: Colors.colorPrimary)
        refreshControl.backgroundColor = Functions.returnUIColorFromHexString(hex: Colors.colorBackground)
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        return refreshControl
    }()
    
    // Variables connect from storyboard
    @IBOutlet var tableView: UITableView!
    @IBOutlet var viewToolbar: UIView!
    
    // Variables
    var chatList: [ChatItem] = Array()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.refreshControl = refresher
        
//        Functions.setNavigationPrimaryBarTint(navigationController: navigationController!)
        Functions.setViewBackgroundPrimaryColor(view: viewToolbar)
        Functions.setViewBackgroundPrimaryColor(view: self.view)
        Functions.setViewBackgroundColor(view: tableView)

        for _ in 0...2 {
            let message = ChatItem()
            chatList.append(message)
        }
        
        tableView.rowHeight = 90
        tableView.dataSource = self
        tableView.delegate   = self
        
        refresher.beginRefreshing()
        DispatchQueue.main.asyncAfter(deadline: .now() + GlobalVariables.delayRequest) {
            self.getChatRequest()
        }
        
    } // end viewDidLoad
    
    @objc private func refreshItems() {
        self.perform(#selector(refreshItemsRequest(_:)), with: nil, afterDelay: GlobalVariables.delayRefresher)
    }
    
    @objc private func refreshItemsRequest(_ sender: Any) {
        getChatRequest()
    }
    
    private func getChatRequest() {
        print("Chat request")
        refresher.endRefreshing()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        super.viewWillDisappear(animated)
    }

} // end class

extension ChatViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "ChatTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatTableViewCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatTableViewCell", for: indexPath) as! ChatTableViewCell
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "Message", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
        navigationController?.pushViewController(viewController, animated: true)
    }
    
} // end extension

