
import UIKit

class ChatTableViewCell: UITableViewCell {

    @IBOutlet var imageViewUser: UIImageView!
    @IBOutlet var imageViewPost: UIImageView!
    @IBOutlet var labelPostTitle: UILabel!
    @IBOutlet var labelUsername: UILabel!
    @IBOutlet var labelMessage: UILabel!
    @IBOutlet var labelDate: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        Functions.customImageViewUser(imageView: imageViewUser)
        Functions.customImageViewPostImage(imageView: imageViewPost)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

} // end class
