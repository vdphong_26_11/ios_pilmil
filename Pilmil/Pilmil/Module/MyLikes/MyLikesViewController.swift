
import UIKit

class MyLikesViewController: UIViewController {
    
    @IBOutlet var btnClose: UIButton!
    @IBOutlet var collectionView: UICollectionView!
    
    // Variables
    var sellItemList: [SellItem] = Array()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Functions.setViewBackgroundPrimaryColor(view: self.view)
        
//        for _ in 0...10 {
//            
//            let item = SellItem(
//                image: UIImage(named: "11")!,
//                title: "i5-4590, RAM 8GB, GTX 1050Ti, SSD 240GB",
//                price: "9.000.000 đ",
//                timeAgo: "1 ngày trước",
//                address: "CT3A Nam Cường, Khu đô thị Cổ Nhuế, Cổ Nhuế 1, Từ Liêm, Hà Nội",
//                description: "Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.",
//                city: "Hà Nội",
//                byUser: "vdphong_26_11",
//                byUserPhoto: UIImage(named: "user")!,
//                numberOfLikes: String(GlobalVariables.numberOfLikes),
//                boolIsSold: false)
//            
//            sellItemList.append(item)
//        }
        
        // Custom CollectionView
        collectionView?.contentInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        collectionView.dataSource = self
        collectionView.delegate   = self

        // Do any additional setup after loading the view.
    } // end viewDidLoad
    
    @IBAction func btnClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        super.viewWillDisappear(animated)
    }
    
} // end class

extension MyLikesViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sellItemList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(UINib(nibName: "ItemCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ItemCollectionViewCell")
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemCollectionViewCell", for: indexPath) as! ItemCollectionViewCell
        cell.sellItem = sellItemList[indexPath.item]
//        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Clicked")
        let sellItem = sellItemList[indexPath.row]
    
        let viewController = UIStoryboard.init(name: "SingleItem", bundle: nil).instantiateViewController(withIdentifier: "SingleItemViewController") as! SingleItemViewController
        viewController.sellItem = sellItem
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemSize = (collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right + 8)) / 2
        return CGSize(width: itemSize, height: itemSize * CGFloat(GlobalVariables.heightFactor))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 7
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 7
    }
    
} // end extension
