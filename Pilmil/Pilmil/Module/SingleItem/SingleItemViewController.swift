
import UIKit

class SingleItemViewController: UIViewController {
    
    // Variable get from BrowseViewController
    var sellItem: SellItem!
    
    // Variables connect from storyboard
    @IBOutlet var heightViewContainer: NSLayoutConstraint!
    @IBOutlet var viewSeller: UIView!
    @IBOutlet var imageViewUser: UIImageView!
    @IBOutlet var viewBottomButtons: UIView!
    @IBOutlet var viewShadow: UIView!
    @IBOutlet var btnBack: UIButton!
    
    @IBOutlet var imageViewItem: UIImageView!
    @IBOutlet var labelItemTitle: UILabel!
    @IBOutlet var labelItemPrice: UILabel!
    @IBOutlet var labelItemTimeAgo: UILabel!
    @IBOutlet var labelItemAddress: UILabel!
    @IBOutlet var labelItemDescription: UILabel!
    
    @IBOutlet var btnLike: UIButton!
    @IBOutlet var btnChat: UIButton!
    
    // Variables
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Functions.setViewBackgroundPrimaryColor(view: self.view)

        // Set if item is sold
        if sellItem.isSold == "true" {
            btnChat.setTitle("Đã bán", for: .normal)
            btnChat.layer.backgroundColor = UIColor.lightGray.cgColor
            btnChat.isUserInteractionEnabled = false
        }
        
        // Set up custom views
        Functions.customImageViewUser(imageView: imageViewUser)
        customViewSeller(view: viewSeller)
        customViewBottomButtons(view: viewBottomButtons)
        customViewShadow(view: viewShadow)
        
        setItem()
        
        let heightOfAllContents =   imageViewItem.frame.height +
                                    labelItemTitle.frame.height +
                                    labelItemPrice.frame.height +
                                    labelItemTimeAgo.frame.height +
                                    labelItemAddress.frame.height +
                                    labelItemDescription.frame.height
        
        let heightOfAllSpaces = ((15 * 8) + 45) + 58 + 10
        let heightOfView = heightOfAllContents + CGFloat(heightOfAllSpaces)
        self.heightViewContainer.constant = heightOfView
        
        // Set btnBack
        btnBack.layer.backgroundColor = UIColor.black.cgColor
        btnBack.layer.opacity = 0.3
        btnBack.layer.cornerRadius = btnBack.frame.height / 2
        let image = UIImage(named: "left_chevron")?.withRenderingMode(.alwaysTemplate)
        btnBack.setImage(image, for: .normal)
        
        // Set swipe right gesture
        let swipeRightGesture = UISwipeGestureRecognizer(target: self, action: #selector(swipeRight))
        swipeRightGesture.direction = .right
        self.view.addGestureRecognizer(swipeRightGesture)
        
    } // end viewDidLoad()
    
    // Actions connect from storyboard
    @IBAction func btnLike(_ sender: UIButton) {
        if GlobalVariables.boolDidLikeItem == false {
            GlobalVariables.numberOfLikes += 1
            btnLike.setTitle(String(GlobalVariables.numberOfLikes), for: .normal)
            btnLike.setImage(UIImage(named: "liked"), for: .normal)
            GlobalVariables.boolDidLikeItem = true
        } else if GlobalVariables.boolDidLikeItem == true {
            GlobalVariables.numberOfLikes -= 1
            btnLike.setTitle(String(GlobalVariables.numberOfLikes), for: .normal)
            btnLike.setImage(UIImage(named: "like"), for: .normal)
            GlobalVariables.boolDidLikeItem = false
        }
    } // end btnLike() action
    
    @IBAction func btnChat(_ sender: Any) {
        let storyboardMessage = UIStoryboard(name: "Message", bundle: nil)
        let viewController = storyboardMessage.instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
        let backItem = UIBarButtonItem()
        backItem.title = "          "
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // Custom Functions
    @objc private func swipeRight() {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func setItem() {
        imageViewItem.image     = sellItem.imageLocal
        labelItemTitle.text     = sellItem.title
        labelItemPrice.text     = sellItem.price
        labelItemTimeAgo.text   = "\(sellItem.timeAgo) tại \(sellItem.city)"
        labelItemAddress.text   = sellItem.address
        labelItemDescription.text = sellItem.description
        sizeToFitLabel(label: labelItemTitle)
        sizeToFitLabel(label: labelItemAddress)
        sizeToFitLabel(label: labelItemDescription)
        
        btnLike.setTitle("\(sellItem.likes)", for: .normal)
    }
    
    
    // Extra Custom Functions
    private func sizeToFitLabel(label: UILabel) {
            label.sizeToFit()
    }
    
    private func customViewSeller(view: UIView) {
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    private func customViewBottomButtons(view: UIView) {
        view.layer.cornerRadius = 5.0
        view.clipsToBounds = true
        if #available(iOS 11.0, *) {
            view.backgroundColor = UIColor(named: "colorLightestGrey")
        } else {
            view.backgroundColor = UIColor.lightGray
        }
    }
    
    private func customViewShadow(view: UIView) {
        view.layer.cornerRadius = 5.0
        view.layer.masksToBounds = false
        view.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
    }

    func setStatusBarBackgroundColor(color: UIColor) {
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        statusBar.backgroundColor = color
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        super.viewWillDisappear(animated)
    }
    
} // end class
