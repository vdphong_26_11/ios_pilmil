
import UIKit
import SwiftyJSON

class MeViewController: UIViewController {
    
    var user: User!
    let loadingIndicator = UIActivityIndicatorView()
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = Functions.returnUIColorFromHexString(hex: Colors.colorPrimary)
        refreshControl.backgroundColor = Functions.returnUIColorFromHexString(hex: Colors.colorBackground)
        refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
        return refreshControl
    }()

    @IBOutlet var collectionViewMe: UICollectionView!
    @IBOutlet var btnLike: UIButton!
    @IBOutlet var btnSetting: UIButton!
    
    var sellItemList: [SellItem] = Array()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Functions.setLoading(activityIndicator: loadingIndicator, view: self.view)
        
        customButton(imageName: "settings", button: btnSetting)
        customButton(imageName: "like_setting", button: btnLike)
        
        collectionViewMe?.contentInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        collectionViewMe.dataSource = self
        collectionViewMe.delegate   = self
        
        // Add Refresh Control to UICollectionView & Configure Refresh Control
        collectionViewMe.refreshControl = refresher
        
        refresher.beginRefreshing()
        DispatchQueue.main.asyncAfter(deadline: .now() + GlobalVariables.delayRequest) {
            self.getUserItems()
        }

    } // end viewDidLoad()
    
    // Button actions from storyboard
    @IBAction func btnLike(_ sender: UIButton) {
        let viewController = UIStoryboard.init(name: "MyLikes", bundle: nil).instantiateViewController(withIdentifier: "MyLikesViewController") as! MyLikesViewController
        let navViewController = UINavigationController.init(rootViewController: viewController)
        self.present(navViewController, animated: true, completion: nil)
    }
    
    @IBAction func btnSettings(_ sender: UIButton) {
        let viewController = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    // Custom Functions
    @objc func tapViewFaces() {
        let viewController = UIStoryboard.init(name: "Feedbacks", bundle: nil).instantiateViewController(withIdentifier: "FeedbacksViewController") as! FeedbacksViewController
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    private func customButton(imageName: String, button: UIButton) {
        let imageSetting = UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate)
        button.setImage(imageSetting, for: .normal)
        button.tintColor = UIColor.white
    }
    
    @objc private func refreshItems() {
        self.perform(#selector(refreshItemsRequest(_:)), with: nil, afterDelay: GlobalVariables.delayRefresher)
    }
    
    @objc private func refreshItemsRequest(_ sender: Any) {
        getUserItems()
    }
    
    private func getUserItems() {

        Request.getUserItems(viewController: self, userId: "999999") { completion in
            
            print("Me requested")
            self.sellItemList.removeAll()
            
            let data: JSON      = completion["data"]
            let items: [JSON]   = data["items"].array!
            
            if (!items.isEmpty || !(items.count == 0)) {
                for i in 0..<items.count {
                    
                    let item  = SellItem(item: items[i])
                    self.sellItemList.append(item)
                    
                } // end for-loop
            } else {
                Functions.createLabel(text: "Bạn chưa bán gì", view: self.collectionViewMe)
            }
            
            self.refresher.endRefreshing()
            self.collectionViewMe.reloadData()
            
        } // end request
        
    } // end getUserItems()
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        super.viewWillDisappear(animated)
    }
    
} // end class

extension MeViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let sectionHeaderView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "SectionHeaderView", for: indexPath) as! SectionHeaderView
            
            sectionHeaderView.frame = CGRect(x: -8 , y: -8, width: self.view.frame.width, height: 140)
            sectionHeaderView.setData(view: self, user: user)
            return sectionHeaderView

        default:  fatalError("Unexpected element kind")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sellItemList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionViewMe.register(UINib(nibName: "ItemCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ItemCollectionViewCell")
        let cell = collectionViewMe.dequeueReusableCell(withReuseIdentifier: "ItemCollectionViewCell", for: indexPath) as! ItemCollectionViewCell
        cell.sellItem = sellItemList[indexPath.item]
        //        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let sellItem = sellItemList[indexPath.row]
        let storyboard = UIStoryboard.init(name: "SingleItem", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SingleItemViewController") as! SingleItemViewController
        viewController.sellItem = sellItem
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemSize = (collectionViewMe.frame.width - (collectionViewMe.contentInset.left + collectionViewMe.contentInset.right + 8)) / 2
        
        return CGSize(width: itemSize, height: itemSize * CGFloat(GlobalVariables.heightFactor))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 7
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 7
    }
    
} // end extension


class SectionHeaderView: UICollectionReusableView {
    
    var view: UIViewController!
    var user: User!

    @IBOutlet var imageViewUserPhoto: UIImageView!
    @IBOutlet var labelUsername: UILabel!
    @IBOutlet var labelUserJoinedDate: UILabel!
    @IBOutlet var imageViewFaceHappy: UIImageView!
    @IBOutlet var labelFaceHappy: UILabel!
    @IBOutlet var imageViewFaceSad: UIImageView!
    @IBOutlet var labelFaceSad: UILabel!
    @IBOutlet var imageViewRightChevron: UIImageView!
    @IBOutlet var viewFaces: UIView!
    
    override func awakeFromNib() {
        // Custom user photo
        imageViewUserPhoto.clipsToBounds = true
        imageViewUserPhoto.layer.cornerRadius = imageViewUserPhoto.frame.height / 2
        imageViewUserPhoto.layer.borderWidth  = 2.0
        imageViewUserPhoto.layer.borderColor  = UIColor.white.cgColor
        
        // Set faces
        viewFaces.clipsToBounds = true
        viewFaces.layer.cornerRadius = 5.0
        viewFaces.layer.borderWidth = 1.0
        viewFaces.layer.borderColor = UIColor.white.cgColor
        viewFaces.backgroundColor = UIColor.white
        
        // Set face right chevron
        let imageRightChevron = UIImage(named: "right_chevron")?.withRenderingMode(.alwaysTemplate)
        imageViewRightChevron.image = imageRightChevron
        imageViewRightChevron.tintColor = Functions.returnUIColorFromHexString(hex: Colors.colorDarkGrey)
        
        imageViewFaceHappy.image = UIImage(named: "happy")?.withRenderingMode(.alwaysTemplate)
        imageViewFaceHappy.tintColor = Functions.returnUIColorFromHexString(hex: Colors.colorDarkGrey)
        imageViewFaceSad.image = UIImage(named: "sad")?.withRenderingMode(.alwaysTemplate)
        imageViewFaceSad.tintColor = Functions.returnUIColorFromHexString(hex: Colors.colorDarkGrey)
        
        // Create tap gesture for feedback faces
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapViewFaces))
        viewFaces.addGestureRecognizer(tapGesture)
        
    } // end awakeFromNib()
    
    func setData(view: UIViewController, user: User) {
        self.view = view
        self.user = user
        
        // Set user photo
        self.imageViewUserPhoto.image = UIImage(named: "myPhoto")
        
        // Set username
        if user.username == "" {
            self.labelUsername.text = user.userName
        } else {
            self.labelUsername.text = user.username
        }
        
        // Set user createDate
        self.labelUserJoinedDate.text = user.createDate
        
        // Set feedback counts
        self.labelFaceHappy.text    = "22"
        self.labelFaceSad.text      = "0"

    }
    
    // Custom Functions
    @objc func tapViewFaces() {
        let viewController = UIStoryboard.init(name: "Feedbacks", bundle: nil).instantiateViewController(withIdentifier: "FeedbacksViewController") as! FeedbacksViewController
        self.view.navigationController?.pushViewController(viewController, animated: true)
    }
    
} // end class
