
import UIKit
import FacebookLogin
import FacebookCore
import SVProgressHUD

class LoginViewController: UIViewController {

    @IBOutlet var btnFacebookLogin: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add gradient background color to super view
//        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height / 2))
        let gradient = CAGradientLayer()
        gradient.frame = self.view.bounds
        gradient.colors = [UIColor.white.cgColor, Functions.returnUIColorFromHexString(hex: "#2FA705").cgColor]
        self.view.layer.insertSublayer(gradient, at: 0)

        btnFacebookLogin.layer.cornerRadius = 7.0
        
    } // end viewDidLoad()
    
    
    // Actions button from storyboard
    @IBAction func btnFacebookLogin(_ sender: UIButton) {
        
        // Once the button is clicked, show the login dialog
        LoginManager().logIn(readPermissions: [ReadPermission.publicProfile, .email, .userFriends, .userBirthday], viewController: self) { loginResult in
            SVProgressHUD.show()
            switch loginResult {
            case .failed(let error):
                SVProgressHUD.showError(withStatus: "\(error)")
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                    SVProgressHUD.dismiss()
                }

            case .cancelled:
                print("User cancelled login.")
                SVProgressHUD.dismiss()

            case .success( _, _, _):
//                print("---LOGGED-IN: \n \(grantedPermissions)\n \(declinedPermissions)\n \(accessToken) \n END---")
                let request = GraphRequest(graphPath: "me", parameters: ["fields": "id, email, name, first_name, last_name, picture.type(large)"], accessToken: AccessToken.current, httpMethod: .GET, apiVersion: FacebookCore.GraphAPIVersion.defaultVersion)
                request.start { (response, result) in
                    switch result {
                        
                    case .failed(let error):
                        SVProgressHUD.showError(withStatus: "\(error)")
                        
                    case .success(let value):

                        let date = Date()
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "dd.MM.yyyy"
                        let createDate = dateFormatter.string(from: date)
                        
                        let user = User(userId: (AccessToken.current?.userId)!, username: "", email: value.dictionaryValue!["email"]! as! String, userName: value.dictionaryValue!["name"]! as! String, createDate: createDate)
                        
                        // Save last logged-in facebook user
                        let encodedUser = try! JSONEncoder().encode(user)
                        UserDefaults.standard.set(encodedUser, forKey: "lastUser")
                        
                        let viewController  = UIStoryboard(name: "MainTabBar", bundle: nil).instantiateViewController(withIdentifier: "MainTabBarController") as! MainTabBarController
                        viewController.user = user
                        self.present(viewController, animated: true, completion: nil)
                        
                    }
                }

            }
        }
        
    } // end btnFacebookLogin()
    
    // Custom functions
    func getUserProfile() {
        print("-----GET USER PROFILE")
        
        let connection = GraphRequestConnection()
        connection.add(GraphRequest(graphPath: "/me")) { httpResponse, result in
            switch result {
            case .success(let response):
                print("-----Graph Request Succeeded \(response) \n END-----")

            case .failed(let error):
                print("-----Graph Request Failed \(error) \n END-----")
            }
        }
        connection.start()

    } // end getUserProfile()
    
    override func viewWillAppear(_ animated: Bool) {
        if let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as? UIView{
            statusBar.isHidden = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as? UIView{
            statusBar.isHidden = false
        }
        SVProgressHUD.dismiss()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        SVProgressHUD.dismiss()
    }
    
} // end class


struct MyProfileRequest: GraphRequestProtocol {
    struct Response: GraphResponseProtocol {
        init(rawResponse: Any?) {
            // Decode JSON from rawResponse into other properties here.
        }
    }
    
    var graphPath = "/me"
    var parameters: [String : Any]? = ["fields": "id, name"]
    var accessToken = AccessToken.current
    var httpMethod: GraphRequestHTTPMethod = .GET
    var apiVersion: GraphAPIVersion = .defaultVersion
}

