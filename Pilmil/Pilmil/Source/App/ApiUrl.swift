
import Foundation

class ApiUrl {
    
    private static let http     = "http://"
    private static let server   = "localhost:8888/api"
//    private static let server   = "localhost:8888/slimmampp/public/api"
    
    public static let apiAddUser        = "/user/add"
    public static let apiGetUser        = "/user/userId="
    public static let apiGetItems       = "/items"
    public static let apiGetUserItems   = "/user/items"
    
    public static func returnServerUrl(apiSuffix: String) -> String { return "\(self.http)\(self.server)\(apiSuffix)"}
    
} // end class
