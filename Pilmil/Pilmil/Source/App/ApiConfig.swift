
import Foundation

public class ApiConfig {
    
    // Server API
    public static var ADD_USER_URL          = ApiUrl.returnServerUrl(apiSuffix: ApiUrl.apiAddUser)
    public static var GET_USER_URL          = ApiUrl.returnServerUrl(apiSuffix: ApiUrl.apiGetUser)
    public static var GET_ITEMS_URL         = ApiUrl.returnServerUrl(apiSuffix: ApiUrl.apiGetItems)
    public static var GET_USER_ITEMS_URL    = ApiUrl.returnServerUrl(apiSuffix: ApiUrl.apiGetUserItems)
    
    public static var accept   = "application/json"
    public static var appID    = "Mr2n4FaCO8XdS7K6x4sHbIT6L+Gumltq7dy/EW0eIXQ="
    public static var devid    = "-1"
    
    /* Playbacks static values */
    public static var playbackTimeStart = "00:00:01"
    public static var playbackTimeEnd   = "23:59:59"
    
}

