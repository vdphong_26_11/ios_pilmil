
import Foundation

public class MessageConfig {
    
    // Alamofire request network error
    public static let errorCommonTitle = "Mất kết nối"
    public static let errorNetwork = "Không thể kết nối mạng... Vui lòng thử lại !"
    public static let errorServer  = "Không tìm thấy server... Vui lòng thử lại !"
    public static let errorAuth    = "Không thể kết nối... Vui lòng thử lại !"
    public static let errorParse   = "Lỗi phân tích JSON... Vui lòng thử lại !"
    public static let errorTimeout = "Hết thời gian... Vui lòng thử lại !"
    
    // for login
    public static let emptyLoginAlertTitle  = "Thông tin trống"
    public static let emptyLoginAlertMsg    = "Vui lòng điền thông tin tài khoản!"
    public static let successLoginAlertMsg  = "Đăng nhập thành công!"
    public static let falseLoginAlertTitle  = "Sai tài khoản"
    public static let falseLoginAlertMsg    = "Vui lòng kiểm tra lại tài khoản!"
    public static let accountNotExistTitle  = "Lỗi đăng nhập"
    public static let accountNotExistMsg    = "Tài khoản không tồn tại!"
    public static let passwordIncorrectTitle = "Lỗi đăng nhập"
    public static let passwordIncorrectMsg   = "Mật khẩu không chính xác!"
    public static let statusErrorTittle     = "Lỗi status"
    public static let statusErrorMsg        = "Status is not 200 hay 404!"
    public static let errorServerTitle      = "Chọn máy chủ"
    public static let errorSerrverMsg       = "Vui lòng chọn máy chủ!"
    
    // for all
    public static let undefinedLogin       = "Tài khoản này vừa  nhập mới";
    public static let loadingToastMsg      = "Đang tải dữ liệu...";
    public static let geocoderErrorTitle   = "Lỗi cập nhật địa chỉ";
    public static let geocoderErrorMsg     = "Vui lòng khởi động lại thiết bị";
    
    // for playbacks and tracking and common requests
    public static let noDataAlertTitle      = "Không có dữ liệu";
    public static let noDataAlertMsg        = "Vui lòng chọn ngày khác!";
    
    // for carlist
    public static let expiredLicenseAlertTitle = "Thiết bị hết hạn dịch vụ";
    public static let expiredLicenseAlertMsg   = "Vui lòng liên hệ CSKH để gia hạn dịch vụ. Xin cảm ơn! \n SDT: 0906.245.155";
    public static let statusRun                = "Đang chạy";
    public static let statusStop               = "Đang dừng";
    public static let statusLostGprs           = "Mất tín hiệu";
    public static let statusLostGps            = "Mất GPS";
    public static let noLicensePlate           = "Không biển số";
    
    // feedback form
    public static let emptyFeedbackAlertTitle  = "Thông tin trống";
    public static let emptyFeedbackAlertMsg    = "Bạn hãy kiểm tra lại thông tin phản hồi!";
    public static let successSendMsg           = "Phản hồi đã được gửi";
    
    // for report
    public static let noFuelSensor   = "Xe chưa lắp cảm biến nhiên liệu";
    public static let noCameraSensor = "Xe chưa lắp camera";
    
}
