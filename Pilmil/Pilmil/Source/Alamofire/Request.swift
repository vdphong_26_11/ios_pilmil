
import Alamofire
import SwiftyJSON
import Foundation
import SVProgressHUD

class Request {
    
    public static let intervalTimedOut:Double = 5
    
    public static func getUserItems (viewController: UIViewController, userId: String, complete: @escaping (_ reviews: JSON) -> ()) {
    
        let parameters: Parameters  = ["userId": userId]
        
        if Connectivity.isConnectedToInternet {
            
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = Request.intervalTimedOut
            manager.request(ApiConfig.GET_USER_ITEMS_URL, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil).validate(statusCode: 200..<600).responseJSON { response in
                
                switch (response.result) {
                case .success:
                    if response.result.value != nil {
                        
                        let result = JSON(response.result.value!)
                        complete(result)
                        
                    } // end if response.result.value != nil
                    break
                    
                case .failure(let error) :
                    if error._code == NSURLErrorTimedOut {
                        Functions.createAlert(title: MessageConfig.errorCommonTitle, message: MessageConfig.errorTimeout, viewController: viewController)
                    }
                    Functions.printError(error: "\(error)")
                    SVProgressHUD.showDismissableError(with: "\(error)")
                    break
                    
                } // end switch
                
            } // end response in {}
            
        } else {
            Functions.createAlert(title: MessageConfig.errorCommonTitle, message: MessageConfig.errorNetwork, viewController: viewController)
        }
        
        
    } // end trackingItemsRequest()
    
    public static func getSellItems ( viewController: UIViewController, complete: @escaping (_ reviews: JSON) -> ()) {
        
        if Connectivity.isConnectedToInternet {
            
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = Request.intervalTimedOut
            manager.request(ApiConfig.GET_ITEMS_URL, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: nil).validate(statusCode: 200..<600).responseJSON { response in
                
                switch (response.result) {
                case .success:
                    if response.result.value != nil {
                        
                        let result = JSON(response.result.value!)
                        complete(result)
                        
                    } // end if response.result.value != nil
                    break
                    
                case .failure(let error) :
                    if error._code == NSURLErrorTimedOut {
                        Functions.createAlert(title: MessageConfig.errorCommonTitle, message: MessageConfig.errorTimeout, viewController: viewController)
                    }
                    Functions.printError(error: "\(error)")
                    SVProgressHUD.showDismissableError(with: "\(error)")
                    break
                    
                } // end switch
                
            } // end response in {}
            
        } else {
            Functions.createAlert(title: MessageConfig.errorCommonTitle, message: MessageConfig.errorNetwork, viewController: viewController)
        }
        
    } // end trackingItemsRequest()
    
    public static func getUser (userId: String, complete: @escaping (_ reviews: JSON) -> ()) {
        
        if Connectivity.isConnectedToInternet {
            
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = Request.intervalTimedOut
            manager.request("\(ApiConfig.GET_USER_URL)\(userId)", method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: nil).validate(statusCode: 200..<600).responseJSON { response in
                
                switch (response.result) {
                case .success:
                    if response.result.value != nil {
                        
                        let result = JSON(response.result.value!)
                        complete(result)
                        
                    } // end if response.result.value != nil
                    break
                    
                case .failure(let error) :
                    if error._code == NSURLErrorTimedOut {
                        Functions.printError(error: "\(MessageConfig.errorTimeout)")
                    }
                    Functions.printError(error: "\(error)")
                    SVProgressHUD.showDismissableError(with: "\(error)")
                    break
                    
                } // end switch
                
            } // end response in {}
            
        } else {
            Functions.printError(error: "\(MessageConfig.errorNetwork)")
        }
        
    } // end trackingItemsRequest()
    
    public static func addUser (userId: String, username: String, email: String, password: String, user_name: String, createDate: String, viewController: UIViewController, complete: @escaping (_ reviews: JSON) -> ()) {
        
        let parameters: Parameters  = [
            "userId": userId,
            "username": username,
            "email": email,
            "password": password,
            "user_name": user_name,
            "createDate": createDate ]
        
        if Connectivity.isConnectedToInternet {
            
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = Request.intervalTimedOut
            
            manager.request(ApiConfig.ADD_USER_URL, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil).validate().responseJSON { response in
                
                switch (response.result) {
                case .success:
                    if response.result.value != nil {
                        
                        let result = JSON(response.result.value!)
                        complete(result)
                        
                    } // end if response.result.value != nil
                    break
                    
                case .failure(let error) :
                    if error._code == NSURLErrorTimedOut {
                        Functions.createAlert(title: MessageConfig.errorCommonTitle, message: MessageConfig.errorTimeout, viewController: viewController)
                    }
                    print("Auth request failed with error: \(error)")
                    break
                    
                } // end switch
                
                
            } // end response in {}
            
        } else {
            Functions.createAlert(title: MessageConfig.errorCommonTitle, message: MessageConfig.errorNetwork, viewController: viewController)
        }
        
    } // end addUser()
    
    
    public static func trackingItemsRequest (viewController: UIViewController, username: String, token: String, userId: String,TRACKING_ITEMS_URL: String, complete: @escaping (_ reviews: JSON) -> ()) {
        
        let headers: HTTPHeaders = ["Accept": ApiConfig.accept,
                                    "AppID": ApiConfig.appID,
                                    "username" : username,
                                    "token" : token,
                                    "devid" : ApiConfig.devid]
        
        let parameters: Parameters  = ["userId": userId]
        
        if Connectivity.isConnectedToInternet {
            
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = Request.intervalTimedOut
            
            manager.request(TRACKING_ITEMS_URL, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers).validate().responseJSON { response in
                
                switch (response.result) {
                case .success:
                    if response.result.value != nil {
                        
                        let result = JSON(response.result.value!)
                        complete(result)
                        
                    } // end if response.result.value != nil
                    break
                    
                case .failure(let error) :
                    if error._code == NSURLErrorTimedOut {
                        Functions.createAlert(title: MessageConfig.errorCommonTitle, message: MessageConfig.errorTimeout, viewController: viewController)
                    }
                    print("Auth request failed with error: \(error)")
                    break
                    
                } // end switch
                
            } // end response in {}
            
        } else {
            Functions.createAlert(title: MessageConfig.errorCommonTitle, message: MessageConfig.errorNetwork, viewController: viewController)
        }
        
        
    } // end trackingItemsRequest()
    
} // end class

extension SVProgressHUD {
    
    public static func showDismissableError(with status: String) {
        let nc = NotificationCenter.default
        nc.addObserver(
            self, selector: #selector(hudTapped(_:)),
            name: NSNotification.Name.SVProgressHUDDidReceiveTouchEvent,
            object: nil
        )
        nc.addObserver(
            self, selector: #selector(hudDisappeared(_:)),
            name: NSNotification.Name.SVProgressHUDWillDisappear,
            object: nil
        )
        SVProgressHUD.showError(withStatus: status)
        SVProgressHUD.setDefaultMaskType(.clear)
    }
    
    @objc
    private static func hudTapped(_ notification: Notification) {
        SVProgressHUD.dismiss()
        SVProgressHUD.setDefaultMaskType(.none)
    }
    
    @objc
    private static func hudDisappeared(_ notification: Notification) {
        let nc = NotificationCenter.default
        nc.removeObserver(self, name: NSNotification.Name.SVProgressHUDDidReceiveTouchEvent, object: nil)
        nc.removeObserver(self, name: NSNotification.Name.SVProgressHUDWillDisappear, object: nil)
        SVProgressHUD.setDefaultMaskType(.none)
    }
    
}
