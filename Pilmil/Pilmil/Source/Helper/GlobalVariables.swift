

import Foundation

public class GlobalVariables {
    
    public static var boolDidLikeItem   = false
    public static var numberOfLikes     = 10
    public static var heightFactor      = 1.655
    public static var delayRequest      = 0.7
    public static var delayRefresher    = 0.5
    
} // end class
