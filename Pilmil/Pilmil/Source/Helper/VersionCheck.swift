
import Foundation
import Alamofire

class VersionCheck {
    
    public static let shared = VersionCheck()
    
    var newVersionAvailable: Bool?
    var appStoreVersion: String?
    var appStoreNotes: String?
    
    func checkAppStore(callback: ((_ versionAvailable: Bool?, _ version: String?, _ notes: String?)->Void)? = nil) {
        let ourBundleId = Bundle.main.infoDictionary!["CFBundleIdentifier"] as! String
        Alamofire.request("https://itunes.apple.com/lookup?bundleId=\(ourBundleId)&country=vn").responseJSON { response in
            var isNew: Bool?
            var versionStr: String?
            var appRelease: String?
            
            if let json = response.result.value as? NSDictionary,
                let results = json["results"] as? NSArray,
                let entry = results.firstObject as? NSDictionary,
                let appVersion = entry["version"] as? String,
                let appReleaseNotes = entry["releaseNotes"] as? String,
                let ourVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
            {
                isNew = ourVersion != appVersion
                versionStr = appVersion
                appRelease = appReleaseNotes
            }
            
            self.appStoreVersion        = versionStr
            self.newVersionAvailable    = isNew
            self.appStoreNotes          = appRelease
            callback?(isNew, versionStr, appRelease)
        }
    }
} // end class VersionCheck
