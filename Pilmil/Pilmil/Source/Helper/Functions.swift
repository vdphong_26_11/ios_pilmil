
import Foundation
import UIKit
import SVProgressHUD

class Functions {
    
    public static func createLabel(text: String, view: UIView) {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 30))
        label.center = CGPoint(x: view.frame.width/2, y: view.frame.height/2)
        label.textAlignment = .center
        label.text = text
        label.textColor = .gray
        label.font = UIFont(name: "Avenir Next", size: 17)
        view.addSubview(label)
    }
    
    public static func setLoading(activityIndicator: UIActivityIndicatorView, view: UIView) {
        activityIndicator.frame = view.bounds
        activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
        activityIndicator.color = Functions.returnUIColorFromHexString(hex: Colors.colorPrimary)
        activityIndicator.hidesWhenStopped = true
        view.addSubview(activityIndicator)
    }
    
    public static func printError(error: String) {
        print("-----ERROR: \n \(error) \n END-----")
    }
    
    public static func createAlert(title: String, message: String, viewController: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        viewController.present(alert, animated: true, completion: nil)
    }

    
    public static func customBackButton(backButton: UIButton) {
        let image = UIImage(named: "left_chevron")?.withRenderingMode(.alwaysTemplate)
        backButton.setImage(image, for: .normal)
    }
    
//    public static func timeAgoDisplay() -> String {
//        
//        let secondAgo = Int(Date().timeIntervalSinceNow)
//        
//        let minute  = 60
//        let hour    = 60 * minute
//        let day     = 24 * hour
//        let week    = 7 * day
//        let month   = 4 * week
//        
//        return "10 mins ago"
//        
//    } // end timeAgoDisplay()
    
    public static func customDefaultSellButton(button: UIButton) {
        button.isUserInteractionEnabled = false
        button.layer.cornerRadius = 5.0
        if #available(iOS 11.0, *) {
            button.setTitleColor(UIColor(named: "colorLightGrey"), for: .normal)
        } else {
            button.setTitleColor(Functions.returnUIColorFromHexString(hex: "#BABABA"), for: .normal)
        }
//        if #available(iOS 11.0, *) {
//            button.layer.backgroundColor = UIColor(named: "colorPrimaryDark")?.cgColor
//        } else {
//            button.layer.backgroundColor = Functions.returnUIColorFromHexString(hex: "#247F05").cgColor
//        }
    }
    
    public static func customFaces(imageName: String, imageView: UIImageView, color: UIColor) {
        let image = UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate)
        imageView.image = image
        imageView.tintColor = color
    }
    
    public static func customImageViewPostImage(imageView: UIImageView) {
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.layer.borderWidth = 0.5
        imageView.layer.borderColor = UIColor.lightGray.cgColor
        imageView.layer.cornerRadius = 5.0
    }
    
    public static func customImageViewUser(imageView: UIImageView) {
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.layer.borderWidth = 0.5
        imageView.layer.borderColor = UIColor.lightGray.cgColor
        imageView.layer.cornerRadius = imageView.frame.height / 2
    }
    
    
    public static func setNavigationPrimaryBarTint(navigationController: UINavigationController) {
        if #available(iOS 11.0, *) {
            navigationController.navigationBar.barTintColor = UIColor(named: "colorPrimary")
        } else {
            navigationController.navigationBar.barTintColor = self.returnUIColorFromHexString(hex: "#2FA705")
        }
    }
    
    public static func setViewBackgroundPrimaryColor(view: UIView) {
        if #available(iOS 11.0, *) {
            view.backgroundColor = UIColor(named: "colorPrimary")
        } else {
            view.backgroundColor = self.returnUIColorFromHexString(hex: "#2FA705")
        }
    }
    
    public static func setViewBackgroundColor(view: UIView) {
        if #available(iOS 11.0, *) {
            view.backgroundColor = UIColor(named: "colorBackground")
        } else {
            view.backgroundColor = self.returnUIColorFromHexString(hex: "#E5F4F4")
        }
    }
    
    static func returnUIColorFromHexString (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
} // end class
