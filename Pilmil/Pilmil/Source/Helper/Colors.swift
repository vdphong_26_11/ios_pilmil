
import Foundation

class Colors {
    
    public static var colorPrimary      = "#2FA705"
    public static var colorLighterGrey  = "#DBDBDB"
    public static var colorLightGrey    = "#BABABA"
    public static var colorDarkGrey     = "#555555"
    public static var colorBackground   = "#E5F4F4"
    
}
